from django.db import models

# Create your models here.

class Contenido(models.Model):
	clave = models.CharField(max_length=32)
	valor = models.TextField()
	#La siguiente función se va a usar cuando queramos imprimir
	def __str__(self):
		return str(self.id) + ": " + self.clave + "---- " + self.valor
	def tiene_as(self):
		return ('a' in self.valor)

# Es necesario definir la relacion entre contenido y comentario, porque en un contenido puede haber varios comentarios
# A cada comentario hay que decirle a qué contenido pertenece, por eso hay que cambiar la tabla de comentarios (llave foreing)

class Comentario(models.Model):
	contenido = models.ForeignKey(Contenido, on_delete=models.CASCADE) # Cuando se borra el 	contenido, que se borren los comentarios
	titulo = models.CharField(max_length=128)
	cuerpo = models.TextField()
	fecha = models.DateTimeField()
	def __str__(self):
		return str(self.id) + ": " + self.titulo

