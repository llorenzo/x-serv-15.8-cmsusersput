from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('loggedIn', views.loggedIn),
    path('logout', views.logout_view),
    path('imagen', views.imagen),
    path('<str:llave>', views.get_content),
    path('', views.index),
    
]


