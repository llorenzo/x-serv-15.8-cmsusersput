from django.http import HttpResponse, Http404
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect

# Create your views here.

formulario = """
No existe valor en la base de datos para esta llave.
<p>Introdúcela:
<p>
<form action="" method="POST">
	Valor: <input type="text" name="valor">
	<br/><input type="submit" value="Enviar">
</form>
"""

@csrf_exempt
def get_content(request, llave):
	if request.method == "PUT":
		valor = request.body.decode('utf-8')
		try:
			c = Contenido.objects.get(clave=llave)
			c.valor = valor
			respuesta = "La llave " + c.clave + " tiene el valor " + c.valor + " y el id " + str(c.id) 
			c.save()
		except Contenido.DoesNotExist:
			if request.user.is_authenticated:
				c = Contenido(clave=llave, valor=valor)
				c.save()
				respuesta = "La llave " + c.clave + " tiene el valor " + c.valor + " y el id " + str(c.id) 	
			else:
				respuesta = "No estás autenticado. <a href='/login'>Haz login!</a>"
		
		return HttpResponse(respuesta)

	#POST
	if request.method == "POST":
		valor = request.POST['valor']
		c = Contenido(clave=llave, valor=valor)
		c.save()
		
	#GET
	try:
		contenido = Contenido.objects.get(clave=llave)
		respuesta = "La llave " + contenido.clave + " tiene el valor " + contenido.valor + " y el id " + str(contenido.id)
	except Contenido.DoesNotExist:
		if request.user.is_authenticated:
			respuesta = formulario
		else:
			respuesta = "No estás autenticado. <a href='/login'>Haz login!</a>"
	return HttpResponse(respuesta)

def index(request):
	#1. Tener la lista de contenidos
	content_list = Contenido.objects.all()
	#2. Cargar la plantilla
	template = loader.get_template('cms/index.html')
	#3- Ligar las variables de la plantilla a las variables de python
	context = {
		'content_list': content_list
	}
	#4. Renderizar -> Ejecutar 3
	return HttpResponse(template.render(context, request))
	
def loggedIn(request):
	if request.user.is_authenticated:
		respuesta = "Logged in as " + request.user.username	
	else:
		respuesta = "No estas autenticado. <a href='/login'>Autenticate</a>"
	return HttpResponse(respuesta) 
	
def logout_view(request):
	logout(request)
	return redirect("/cms/")
	
def imagen(request):
	template = loader.get_template('cms/plantilla.html')
	context = {}
	return HttpResponse(template.render(context,request))	
	

